jQuery(function($){
	var phForms = $('#views-exposed-form-listing-content-listings'),
    	phFields = 'input[type=text]';
	phForms.find(phFields).each(function(){ // loop through each field in the specified form(s)
		var el = $(this), // field that is next in line
		wrapper = el.parents('.form-item'), // parent .form-item div
		lbl = wrapper.find('label'), // label contained in the wrapper
		lblText = lbl.text(); // the label's text

		// add label text to field's placeholder attribute
		el.attr("placeholder",lblText);

		// hide original label
		lbl.hide();

	});
});